OpenCPC™ is the world’s first fully open source, low cost,  condensation particle counter. The design of the OpenCPC focused on simplicity for design, repair and replication. 

For information about OpenAeros and the OpenCPC project, please visit https//[www.openaeros.com/OpenCPC](www.openaeros.com/OpenCPC)

For more information about this project check the  OpenCPC_Documentation repository found at  https://gitlab.com/OpenAeros/OpenCPC_documentation
